# user-exercises-restassured-testing

### Purpose of project
A very simple rest-assured framework for testing user-exercises-rest webservice (referenced in support repositories)

### Installing and Running
Import as gradle project. Run the tests individually via jUnit or run *./gradlew test*

### Supporting Repositories

The below service will need to be running on localhost:8080.

user-exercises-rest: https://bitbucket.org/aaronmwilliams/user-exercises-rest