package user;

import org.junit.Test;

import static io.restassured.RestAssured.given;

public class UserDeleteTest extends AbstractUserTest {

    @Test
    public void shouldDeleteUser() {
        User user = userTestDataCreator.createUser();

        given()
                .contentType("application/json")
        .when()
                .delete(buildUserURL(user.getId()))
        .then()
                .statusCode(204);
    }

    @Test
    public void shouldReturn404IfUserDoesNotExist() {
        given()
                .contentType("application/json")
        .when()
                .delete(buildUserURL(0))
        .then()
                .statusCode(404);
    }

}
