package user;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class UserCreateTest extends AbstractUserTest {

    @Test
    public void shouldCreateNewUser() {

        user = userBuilder.buildRandomUser();

        given()
                .contentType("application/json")
                .body(user)
        .when()
                .post(USER_PATH)
        .then()
                .statusCode(201)
                .body("name", equalTo(user.getName()),
                        "postcode", equalTo(user.getPostcode()));
    }

    @Test
    public void shouldValidateNameLengthTooLong() {

        user = userBuilder.buildUserWithTooLongName();

        given()
                .contentType("application/json")
                .body(user)
        .when()
                .post(USER_PATH)
        .then()
                .statusCode(422)
                .body("error", equalTo("Unable to process input"));
    }

    @Test
    public void shouldValidateNamePostcodeTooLong() {

        user = userBuilder.buildUserWithTooLongPostcode();

        given()
                .contentType("application/json")
                .body(user)
        .when()
                .post(USER_PATH)
        .then()
                .statusCode(422)
                .body("error", equalTo("Unable to process input"));
    }

    @Test
    public void shouldValidateNameNotNull() {

        user = new User("", "AA231DF");

        given()
                .contentType("application/json")
                .body(user)
        .when()
                .post(USER_PATH)
        .then()
                .statusCode(422)
                .body("error", equalTo("Unable to process input"));
    }

    @Test
    public void shouldValidatePostcodeNotNull() {

        user = new User("Aaron Williams", "");

        given()
                .contentType("application/json")
                .body(user)
        .when()
                .post(USER_PATH)
        .then()
                .statusCode(422)
                .body("error", equalTo("Unable to process input"));
    }

    @Test
    public void shouldFailWithInvalidSchema() {

        given()
                .contentType("application/json")
                .body("\"invalid\":\"Invalid\"")
        .when()
                .post(USER_PATH)
        .then()
                .statusCode(400);
    }

}
