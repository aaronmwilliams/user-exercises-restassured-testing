package log;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.isA;

public class LogGetTest extends AbstractLogTest {

    @Test
    public void shouldGetLog() {
        Log log = logTestDataCreator.createLog();

        given()
                .contentType("application/json")
        .when()
                .get(buildLogURL(log.getId()))
        .then()
                .statusCode(200)
                .body("id", isA(Integer.class))
                .body("exerciseId", equalTo(log.getExerciseId()))
                .body("userId", equalTo(log.getUserId()))
                .body("userName", equalTo(log.getUserName()))
                .body("description", equalTo(log.getDescription()))
                .body("date", equalTo(log.getDate()));
    }

}
