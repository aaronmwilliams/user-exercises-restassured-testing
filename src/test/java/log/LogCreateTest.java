package log;

import org.json.simple.JSONObject;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class LogCreateTest extends AbstractLogTest {

    protected final String DATE = logTestDataCreator.getLogDate();

    @Test
    public void shouldCreateNewLog() {
        user = userTestDataCreator.createUser();
        exercise = exerciseTestDataCreator.createExercise();
        log = new Log(user.getId(), user.getName(),
                        exercise.getId(), exercise.getDescription(), DATE);

        given()
                .contentType("application/json")
                .body(log)
        .when()
                .post(LOG_PATH)
        .then()
                .statusCode(201)
                .body("userId", equalTo(user.getId()));
    }

    @Test
    public void shouldFailValidationIfUserDoesNotExist() {
        exercise = exerciseTestDataCreator.createExercise();
        log = new Log(0, "",
                        exercise.getId(), exercise.getDescription(), DATE);

        given()
                .contentType("application/json")
                .body(log)
        .when()
                .post(LOG_PATH)
        .then()
                .statusCode(422);
    }

    @Test
    public void shouldFailValidationIfExerciseDoesNotExist() {
        user = userTestDataCreator.createUser();
        log = new Log(user.getId(), user.getName(),
                            0, "", DATE);

        given()
                .contentType("application/json")
                .body(log)
        .when()
                .post(LOG_PATH)
        .then()
                .statusCode(422);
    }

    @Test
    public void shouldFailValidationIfUserIsMissing() {
        exercise = exerciseTestDataCreator.createExercise();
        JSONObject json = new JSONObject();
        json.put("exerciseId", exercise.getId());
        json.put("date", DATE);

        given()
                .contentType("application/json")
                .body(json)
        .when()
                .post(LOG_PATH)
        .then()
                .statusCode(422);
    }

    @Test
    public void shouldFailValidationIfExerciseIsMissing() {
        user = userTestDataCreator.createUser();
        JSONObject json = new JSONObject();
        json.put("userId", user.getId());
        json.put("date", DATE);
        given()
                .contentType("application/json")
                .body(json)
        .when()
                .post(LOG_PATH)
        .then()
                .statusCode(422);
    }

    @Test
    public void shouldFailValidationIfDateIsMissing() {
        user = userTestDataCreator.createUser();
        exercise = exerciseTestDataCreator.createExercise();
        JSONObject json = new JSONObject();
        json.put("userId", user.getId());
        json.put("exercideId", exercise.getId());

        given()
                .contentType("application/json")
                .body(json)
        .when()
                .post(LOG_PATH)
        .then()
                .statusCode(422);
    }

    @Test
    public void shouldFailValidationIfDateIsInvalidFormat() {
        user = userTestDataCreator.createUser();
        exercise = exerciseTestDataCreator.createExercise();
        log = new Log(user.getId(), user.getName(),
                            exercise.getId(), exercise.getDescription(), "A");

        given()
                .contentType("application/json")
                .body(log)
        .when()
                .post(LOG_PATH)
        .then()
                .statusCode(400);
    }

}
