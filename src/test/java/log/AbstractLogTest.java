package log;

import exercise.Exercise;
import rest.ExerciseTestDataCreator;
import rest.LogTestDataCreator;
import rest.UserTestDataCreator;
import user.User;

public abstract class AbstractLogTest {

    protected static final String LOG_PATH = "/api/log";
    protected UserTestDataCreator userTestDataCreator = new UserTestDataCreator();
    protected User user;
    protected ExerciseTestDataCreator exerciseTestDataCreator = new ExerciseTestDataCreator();
    protected Exercise exercise;
    protected LogTestDataCreator logTestDataCreator = new LogTestDataCreator();

    protected Log log;

    protected String buildLogURL(int logId) {
        return LOG_PATH + "/" + logId;
    }
}
