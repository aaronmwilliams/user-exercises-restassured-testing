package log;

import org.junit.Test;

import static io.restassured.RestAssured.given;

public class LogDeleteTest extends AbstractLogTest {

    protected final String DATE = logTestDataCreator.getLogDate();

    @Test
    public void shouldDeleteLog() {
        log = logTestDataCreator.createLog();

        given()
                .contentType("application/json")
        .when()
                .delete(buildLogURL(log.getId()))
        .then()
                .statusCode(204);
    }

    @Test
    public void shouldReturn404IfLogDoesNotExist() {
        given()
                .contentType("application/json")
        .when()
                .delete(buildLogURL(0))
        .then()
                .statusCode(404);
    }
}
