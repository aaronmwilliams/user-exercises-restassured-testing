package exercise;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class ExerciseCreateTest extends AbstractExerciseTest {

    @Test
    public void shouldCreateNewExercise() {
        exercise = exerciseBuilder.buildRandomExercise();

        given()
                .contentType("application/json")
                .body(exercise)
        .when()
                .post(EXERCISE_PATH)
        .then()
                .statusCode(201)
                .body("description", equalTo(exercise.getDescription()));
    }

    @Test
    public void shouldValidateDescriptionLengthTooLong() {
        exercise = exerciseBuilder.buildExerciseWithTooLongDescription();

        given()
                .contentType("application/json")
                .body(exercise)
        .when()
                .post(EXERCISE_PATH)
        .then()
                .statusCode(422)
                .body("error", equalTo("Unable to process input"));
    }

    @Test
    public void shouldValidateDescriptionNotNull() {
        exercise = new Exercise("");

        given()
                .contentType("application/json")
                .body(exercise)
        .when()
                .post(EXERCISE_PATH)
        .then()
                .statusCode(422)
                .body("error", equalTo("Unable to process input"));
    }

    @Test
    public void shouldFailWithInvalidSchema() {
        given()
                .contentType("application/json")
                .body("")
        .when()
                .post(EXERCISE_PATH)
        .then()
                .statusCode(400);
    }

}
