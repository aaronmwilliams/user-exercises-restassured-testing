package exercise;

import org.junit.Test;

import static io.restassured.RestAssured.when;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsEqual.equalTo;

public class ExerciseGetTest extends AbstractExerciseTest {

    @Test
    public void shouldGetExercise() {
        exercise = exerciseTestDataCreator.createExercise();

        when()
                .get(buildExerciseURL(exercise.getId()))
        .then()
                .statusCode(200)
                .body("description", equalTo(exercise.getDescription()));
    }

    @Test
    public void shouldReturn404ForUnknownExercise() {
        when()
                .get(buildExerciseURL(0))
        .then()
                .statusCode(404);
    }

    @Test
    public void shouldFailValidationForInvalidURL() {
        when()
                .get(EXERCISE_GET_PATH + "A")
        .then()
                .statusCode(400);
    }

    @Test
    public void shouldGetAllExercise() {
        Exercise firstExercise = exerciseTestDataCreator.createExercise("Test Skipping");
        Exercise secondExercise = exerciseTestDataCreator.createExercise("Test Punch Bag");

        when()
                .get(EXERCISE_GET_PATH)
        .then()
                .body("findAll {it.description.contains('Test')}.description",
                        hasItems(firstExercise.getDescription(),
                                secondExercise.getDescription()));
    }

}