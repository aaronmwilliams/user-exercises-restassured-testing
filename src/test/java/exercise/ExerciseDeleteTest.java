package exercise;

import org.junit.Test;

import static io.restassured.RestAssured.given;


public class ExerciseDeleteTest extends AbstractExerciseTest {

    @Test
    public void shouldDeleteExercise() {
        Exercise exercise = exerciseTestDataCreator.createExercise();

        given()
                .contentType("application/json")
        .when()
                .delete(buildExerciseURL(exercise.getId()))
        .then()
                .statusCode(204);
    }

    @Test
    public void shouldReturn404IfExerciseDoesNotExist() {
        given()
                .contentType("application/json")
        .when()
                .delete(buildExerciseURL(0))
        .then()
                .statusCode(404);
    }

}
