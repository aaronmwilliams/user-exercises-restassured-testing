package exercise;

import rest.ExerciseTestDataCreator;

public abstract class AbstractExerciseTest {

    protected static final String EXERCISE_PATH = "/api/exercises";
    protected static final String EXERCISE_GET_PATH = "/api/exercises/";

    protected ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
    protected ExerciseTestDataCreator exerciseTestDataCreator = new ExerciseTestDataCreator();
    protected Exercise exercise;

    protected String buildExerciseURL(int exerciseId) {
        return EXERCISE_PATH + "/" + exerciseId;
    }

}
